var Canvas = require('canvas')
  , Image = Canvas.Image
  , canvas = new Canvas(200, 200)
  , ctx = canvas.getContext('2d');

fs.readFile(__dirname + '../www/res/maps/map2_test1.json', function(err, f) {
    var map_json = JSON.parse(f);

    map_json['layers']
});

fs.readFile(__dirname + '../www/res/tilesets/squid.png', function(err, squid) {
  if (err) throw err;
  img = new Image;
  img.src = squid;
  ctx.drawImage(img, 0, 0, img.width / 4, img.height / 4);
});

// ctx.font = '30px Impact';
// ctx.rotate(.1);
// ctx.fillText("Awesome!", 50, 100);

// var te = ctx.measureText('Awesome!');
// ctx.strokeStyle = 'rgba(0,0,0,0.5)';
// ctx.beginPath();
// ctx.lineTo(50, 102);
// ctx.lineTo(50 + te.width, 102);
// ctx.stroke();

console.log('<img src="' + canvas.toDataURL() + '" />');