/*
       Licensed to the Apache Software Foundation (ASF) under one
       or more contributor license agreements.  See the NOTICE file
       distributed with this work for additional information
       regarding copyright ownership.  The ASF licenses this file
       to you under the Apache License, Version 2.0 (the
       "License"); you may not use this file except in compliance
       with the License.  You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing,
       software distributed under the License is distributed on an
       "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
       KIND, either express or implied.  See the License for the
       specific language governing permissions and limitations
       under the License.
 */

package com.eight_bit_heroes.krak_legends;

import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import com.kontakt.sdk.android.ble.configuration.ActivityCheckConfiguration;
import com.kontakt.sdk.android.ble.configuration.ForceScanConfiguration;
import com.kontakt.sdk.android.ble.configuration.ScanPeriod;
import com.kontakt.sdk.android.ble.configuration.scan.EddystoneScanContext;
import com.kontakt.sdk.android.ble.configuration.scan.IBeaconScanContext;
import com.kontakt.sdk.android.ble.configuration.scan.ScanContext;
import com.kontakt.sdk.android.ble.connection.OnServiceReadyListener;
import com.kontakt.sdk.android.ble.discovery.BluetoothDeviceEvent;
import com.kontakt.sdk.android.ble.discovery.EventType;
import com.kontakt.sdk.android.ble.discovery.eddystone.EddystoneDeviceEvent;
import com.kontakt.sdk.android.ble.discovery.ibeacon.IBeaconAdvertisingPacket;
import com.kontakt.sdk.android.ble.discovery.ibeacon.IBeaconDeviceEvent;
import com.kontakt.sdk.android.ble.filter.eddystone.EddystoneFilters;
import com.kontakt.sdk.android.ble.filter.ibeacon.IBeaconFilter;
import com.kontakt.sdk.android.ble.manager.ProximityManager;
import com.kontakt.sdk.android.ble.rssi.RssiCalculators;
import com.kontakt.sdk.android.ble.util.BluetoothUtils;
import com.kontakt.sdk.android.common.KontaktSDK;
import com.kontakt.sdk.android.common.Proximity;
import com.kontakt.sdk.android.common.log.LogLevel;
import com.kontakt.sdk.android.common.profile.IBeaconDevice;
import com.kontakt.sdk.android.common.profile.IBeaconRegion;
import com.kontakt.sdk.android.common.profile.IEddystoneDevice;
import com.kontakt.sdk.android.common.profile.IEddystoneNamespace;
import org.apache.cordova.BuildConfig;
import org.apache.cordova.CordovaActivity;

import java.util.*;
import java.util.concurrent.TimeUnit;

public class MainActivity extends CordovaActivity implements ProximityManager.ProximityListener
{
    private List<String> discoveredBeacons;
    private Map<String, Place> places;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        discoveredBeacons = new ArrayList<String>();
        places = new HashMap<String, Place>();

        // Known devices
        // `key` = "#{major}/#{minor}"

        // biały
        places.put("20558/17629", new Place("Pomnik psa Dżoka", "Dżok the dog was waiting on this place for his master, who suddenly died, for <b>one year</b>"));

        // czarny
        places.put("33301/31613", new Place("Zakrzówek", "Zakrzówek is a bay on a place of an old <b>quarry</b> with a beautiful sights."));

        initializeKontaktSDK();

        // Set by <content src="index.html" /> in config.xml
        loadUrl(launchUrl);
    }

    private void initializeKontaktSDK() {
        KontaktSDK.initialize(this)
                .setDebugLoggingEnabled(BuildConfig.DEBUG)
                .setLogLevelEnabled(LogLevel.DEBUG, true)
                .setCrashlyticsLoggingEnabled(true);

        proximityManager = new ProximityManager(this);

        IBeaconScanContext iBeaconScanContext = new IBeaconScanContext.Builder()
                .setEventTypes(EnumSet.of(EventType.DEVICE_DISCOVERED,
                        EventType.DEVICE_LOST))
                .setRssiCalculator(RssiCalculators.newLimitedMeanRssiCalculator(5))
                .setIBeaconFilters(Arrays.asList(
                        new IBeaconFilter() {
                            @Override
                            public boolean apply(IBeaconAdvertisingPacket iBeaconAdvertisingPacket) {
                                return iBeaconAdvertisingPacket.getProximity() == Proximity.NEAR;
                            }
                        }
//                        ,
//                        IBeaconFilters.newMajorFilter(33)
                ))
                .build();

        EddystoneScanContext eddystoneScanContext = new EddystoneScanContext.Builder()
                .setEventTypes(EnumSet.of(EventType.SPACE_ENTERED,
                        EventType.SPACE_ABANDONED))
                .setUIDFilters(Arrays.asList(
                        EddystoneFilters.newUIDFilter(KontaktSDK.DEFAULT_KONTAKT_EDDYSTONE_NAMESPACE_ID, "000013")
                ))
                .setURLFilters(Arrays.asList(
                        EddystoneFilters.newURLFilter("http://Kontakt.io")
                ))
                .build();


        scanContext = new ScanContext.Builder()
                .setScanMode(ProximityManager.SCAN_MODE_BALANCED)
                .setIBeaconScanContext(iBeaconScanContext)
                .setEddystoneScanContext(eddystoneScanContext)
                .setActivityCheckConfiguration(ActivityCheckConfiguration.DEFAULT)
                .setForceScanConfiguration(ForceScanConfiguration.DEFAULT)
                .setScanPeriod(new ScanPeriod(TimeUnit.SECONDS.toMillis(5), 0))
                .build();
    }

    private static final int REQUEST_CODE_ENABLE_BLUETOOTH = 1;

    private ProximityManager proximityManager;

    private ScanContext scanContext;

    @Override
    protected void onResume() {
        super.onResume();

        if (!BluetoothUtils.isBluetoothEnabled()) {
            final Intent intent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(intent, REQUEST_CODE_ENABLE_BLUETOOTH);
        } else {
            initializeScan();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        proximityManager.finishScan();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        switch(requestCode) {

            case REQUEST_CODE_ENABLE_BLUETOOTH:

                if(resultCode == RESULT_OK) {
                    initializeScan();
                }
                break;

            default:
                super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onScanStart() {

    }

    @Override
    public void onEvent(BluetoothDeviceEvent event) {
        switch (event.getDeviceProfile()) {

            case IBEACON:
                final IBeaconDeviceEvent iBeaconDeviceEvent = (IBeaconDeviceEvent) event;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        IBeaconRegion region = iBeaconDeviceEvent.getRegion();
                        List<IBeaconDevice> deviceList = iBeaconDeviceEvent.getDeviceList();

                        for (IBeaconDevice device : deviceList) {
                            // String uuid = device.getProximityUUID().toString();
                            String key = String.format("%s/%s", device.getMajor(), device.getMinor());

                            if (discoveredBeacons.contains(key))
                                continue;

                            discoveredBeacons.add(key);

                            Place place = null;

                            if (places.containsKey(key)) {
                                place = places.get(key);
                            } else {
                                continue;
                            }

                            appView.sendJavascript(String.format("window.app.showPopup('You have found %s!<br/>%s');", place.getTitle(), place.getStory()));
                        }
                    }
                });
                break;

            case EDDYSTONE:
                final EddystoneDeviceEvent eddystoneDeviceEvent = (EddystoneDeviceEvent) event;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        IEddystoneNamespace namespace = eddystoneDeviceEvent.getNamespace();
                        List<IEddystoneDevice> deviceList = eddystoneDeviceEvent.getDeviceList();
                    }
                });
                break;

            default:

        }
    }

    @Override
    public void onScanStop() {

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        proximityManager.disconnect();
        proximityManager = null;
    }

    private void initializeScan() {
        proximityManager.initializeScan(scanContext, new OnServiceReadyListener() {
            @Override
            public void onServiceReady() {
                proximityManager.attachListener(MainActivity.this);
            }

            @Override
            public void onConnectionFailure() {
                // Utils.showToast(RangeActivity.this, getString(R.string.unexpected_error_connection));
                Log.e("MOO", "Connection failed");
            }
        });
    }
}
